<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<script src="https://code.jquery.com/jquery-2.2.3.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	<link rel="stylesheet" href="style/stylesheet.css">

</head>
<body>
		<jsp:include page="profielheader.jsp"/>
		<%@page import="java.util.*"%>
		<%@page import="voeding.model.*" %>
		<%@page import="persistence.*" %>
		
		<%
		List<GekozenProduct> producten = null;
		//List<GekozenProduct> gekozenProducten = null;
		Gebruiker g = null;
		
		if (request.getSession().getAttribute("loggedUser") instanceof Gebruiker) {
			g = (Gebruiker)request.getSession().getAttribute("loggedUser");
		}		
		
		if (g != null) {
			//gekozenProducten = VoedingServiceProvider.getGekozenProductDAO().getGekozenProducten(g.getGebruikerId());
			producten = VoedingServiceProvider.getGekozenProductService().GetByGebruiker(g);
		}
		//request.setAttribute("producten", producten);
		request.setAttribute("producten", producten);
		%>

	
	<div class="container">
			<div class="col s6 offset-s3 m6 offset-m3">
            <div class="card-panel">
              <h4 style="text-align:center">Producten</h4>

            </div>
          </div>
	
		<c:if test="${succes == true}">
		<h5 style="color:green;">Product toegevoegd</h5>
		</c:if>
		<c:if test="${succes == false}">
		<h5 style="color:red;">Product is al toegevoegd</h5>
		</c:if>
		<form action="/ProductServlet.do" method="post">
		<div class="row">
		
		
		<%
		List<Product> producten2 = VoedingServiceProvider.getProductService().getAllProducten();
		
		request.setAttribute("producten2", producten2);
		%>
	
		<c:forEach items="${producten2}" var="product">
		<div class="col s6">
			<div class="card large">
		    	<div class="card-image">
		      		<img class="activator" width="250" height="300" src="/images/${product.naam}.jpg">
		    	</div>
		    <div class="card-content">
		      	<span class="card-title activator grey-text text-darken-4">${product.naam} <i class="material-icons right">Product info</i></span>
		      	

		    </div>
		    <div class="card-action">
		    <i class="material-icons left">Status: ${product.status}</i>
		    <br><i class="material-icons left">Categorie: ${product.categorie}</i>
		    <i class="material-icons right"><button class="btn light-green darken-1" type="submit" value="${product.getProductId()}" name="toevoegen">Toevoegen</button></i>
		    </div>
		    <div class="card-reveal">
		      	<span class="card-title grey-text text-darken-4">${product.naam}<i class="material-icons right">Sluiten</i></span>
		      	<p>${product.omschrijving} </p>
		    </div>
		  </div>

		</div>
		
		 </c:forEach>
		 </div>
		</form>
		
	</div>

</body>
<jsp:include page="profielfooter.jsp"/>
</html>

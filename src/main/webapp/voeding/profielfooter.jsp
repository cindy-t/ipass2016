	     <footer class="page-footer green accent-3">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text"></h5>
                <p class="grey-text text-lighten-4">Welkom op mijn persoonlijke voedingslijst</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Meer informatie</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="/voeding/over.jsp">Over deze webapplicatie</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            � 2016 Copyright 
            </div>
          </div>
        </footer>
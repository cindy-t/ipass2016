<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<script src="https://code.jquery.com/jquery-2.2.3.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	<link rel="stylesheet" href="style/stylesheet.css">
</head>
<body>
	<jsp:include page="profielheader.jsp"/>
	<%@page import="java.util.*"%>
	<%@page import="voeding.model.*" %>
	<%@page import="persistence.*" %>
	<%
		List<GekozenProduct> producten = null;
		//List<GekozenProduct> gekozenProducten = null;
		Gebruiker g = null;
		
		if (request.getSession().getAttribute("loggedUser") instanceof Gebruiker) {
			g = (Gebruiker)request.getSession().getAttribute("loggedUser");
		}		
		
		if (g != null) {
			//gekozenProducten = VoedingServiceProvider.getGekozenProductDAO().getGekozenProducten(g.getGebruikerId());
			producten = VoedingServiceProvider.getGekozenProductService().GetByGebruiker(g);
		}
		//request.setAttribute("producten", producten);
		request.setAttribute("producten", producten);
	%>
	
	<div class="container">
	
	<div class="col s6 offset-s3 m6 offset-m3">
            <div class="card-panel">
              <h4 style="text-align:center">Profiel</h4>

            </div>
          </div>
          
         <div class="row">
	    	<div class="col s4 " style="text-align:center">
      			<div class="z-depth-1"><span class="flow-text">Naam: ${loggedUser.voornaam}<p>Achternaam: ${loggedUser.achternaam}</p>
    			<p>Leeftijd: ${loggedUser.leeftijd}</p></span>
    				<a href="/LogoutServlet.do"><button class="btn-large light-blue darken-4" type="submit" name="action">Uitloggen</button></a>
    			</div>
    		</div>

	    	<div class="col s8"  style="text-align:center">
			<div class="z-depth-1">
			<form action="/ProfielServlet.do" method="post">
			            <div class="card-panel">
              <h5 style="text-align:center">Voedingslijst</h5>

            </div>
				<table class="bordered">
				<thead>
					<tr><th>Naam</th><th>Status</th><th>Categorie</th><th>Actie</th></tr></thead>
					<c:forEach items="${producten}" var="product">
						<tbody>
						<tr>
							<td>${product.product.naam}</td>
							<td>${product.product.status}</td>
							<td>${product.product.categorie}</td>
							<td><button class="btn red lighten-1" name="toDelete" value="${product.getId()}" type="submit">Verwijder</button></td>
						</tr>
						</tbody>
					</c:forEach>
				</table>
			</form>
		</div>
	</div>
	</div>

	</div>

</body>
<jsp:include page="profielfooter.jsp"/>
</html>

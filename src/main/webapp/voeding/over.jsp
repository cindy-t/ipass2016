<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<script src="https://code.jquery.com/jquery-2.2.3.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
</head>
<body>
	<jsp:include page="profielheader.jsp"/>

	
	<div class="container">
	
	<div class="col s6 offset-s3 m6 offset-m3">
          <div class="card-panel">
            <h4 style="text-align:center">Over deze webapplicatie</h4>
          </div>
     </div>
          
     <div class="row">
        <div class="col s6">
          <div class="card blue darken-2">
            <div class="card-content white-text">
              <span class="card-title">IPASS 2016</span>
              <p>Deze webapplicatie is gemaakt door een student die Software and information engineering
              studeert aan de Hogeschool Utrecht. Dit is een eindopdracht van het eerste jaar, om de
              kennis die je hebt opgedaan tijdens de colleges te kunnen toepassen in een individueel project.</p>
            </div>
          </div>
         </div>
        </div>
     </div>

</body>
<jsp:include page="profielfooter.jsp"/>
</html>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<script src="https://code.jquery.com/jquery-2.2.3.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	<link rel="stylesheet" href="style/stylesheet.css">

</head>
<body>
	<jsp:include page="profielheader.jsp"/>
	<%@page import="java.util.*"%>
	<%@page import="voeding.model.*" %>
	<%@page import="persistence.*" %>
	<%
		List<Product> producten = VoedingServiceProvider.getProductService().getAllProducten();
		
		request.setAttribute("producten", producten);
	%>
	
	<div class="container">
		<div class="col s6 offset-s3 m6 offset-m3">
            <div class="card-panel">
              <h4 style="text-align:center">Admin paneel</h4>

            </div>
          </div>

		<div class="row"></div>
		<div class="col s8"  style="text-align:center">
			<div class="z-depth-1">
				<table>
				<thead>
					<tr><th>Naam</th><th>Status</th><th>Categorie</th><th>Omschrijving</th><th>&nbsp;</th></tr></thead>
					<c:forEach items="${producten}" var="product">
						<tr>
							<td>${product.naam}</td>
							<td>${product.status}</td>
							<td>${product.categorie}</td>
							<td>${product.omschrijving}</td>
							<td><button name="bewerken" data-id="${product.getProductId()}" data-naam="${product.naam}"
							data-status="${product.status}" data-categorie="${product.categorie}" data-omschrijving="${product.omschrijving}"  class="edit btn light-blue darken-1">Bewerk</button></td>
						</tr>
					</c:forEach>
				</table>
			</div>	
	</div>
	</div>

  <!-- Modal Structure -->
  <div id="edit" class="modal modal-fixed-footer">
    <div class="modal-content">
      

  <div class="row">
    <form class="col s12">
      <div class="row">
      <input class="id" id="id" type="hidden" data-id="">
        <div class="input-field col s12">
          <input class="naam" id="naam" type="text" class="validate">
          <label for="naam" class="active">Naam</label>
        </div>
        <div class="input-field col s12">
          <input class="status" id="status" type="text" class="validate">
          <label for="status" class="active">Status</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input class="categorie" id="categorie" type="text" class="validate">
          <label for="categorie" class="active">Categorie</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
         <textarea id="omschrijving" class="omschrijving materialize-textarea" length="255"></textarea>
            <label for="omschrijving" class="active">Omschrijving</label>

        </div>
      </div>
    </form>
  </div>
			

    </div>
    <div class="modal-footer">
      <a name="opslaan" class="modal-action opslaan  btn-flat ">Opslaan</a>
      <a name="opslaan" class="modal-action modal-close btn-flat ">Sluiten</a>
    </div>
  </div>
          
	
	<script type="text/javascript">
		$(document).ready(function() {

			$(document).on('click', '.edit', function() {
				console.log("Open modal: " + $(this).data('naam'));
				
				$('label').addClass('active');
				
				$('.id').val($(this).data('id'));
				$('.naam').val($(this).data('naam'));
				$('.status').val($(this).data('status'));
				$('.categorie').val($(this).data('categorie'));
				$('.omschrijving').val($(this).data('omschrijving'));
				
				$('#edit').openModal();
			});
 			$(document).on('click', '.opslaan', function() {
 				var url = "/AdminPaneelServlet.do";
				var data = "opslaan=" + $('#id').val();
				data += "&naam=" + $('#naam').val();
				data += "&status=" + $('#status').val();
				data += "&categorie=" + $('#categorie').val();
				data += "&omschrijving=" + $('#omschrijving').val();
				
				$.ajax({
					url : url,
					type : 'POST',
					data : data,
					success : function(result) {
						console.log('done');
						$('#edit').closeModal();
						window.location.replace("/voeding/adminpanel.jsp");
					}
				});
					
 			});
		});
</script>

	
</body>
<jsp:include page="profielfooter.jsp"/>
</html>

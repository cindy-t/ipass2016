<title>Mijn persoonlijke voedingslijst</title>

		<nav class="nav-wrapper green accent-3" role="navigation">
		<div class="container">
		    <div class="nav-wrapper">
		      <a href="#" class="brand-logo">Mijn persoonlijke voedingslijst</a>
		      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">Menu</i></a>
		      
		      <ul id="nav-mobile" class="right hide-on-med-and-down">
		        <li><a href="/voeding/profiel.jsp">Profiel</a></li>
		        <li><a href="/voeding/producten.jsp">Producten</a></li>
		        <li><a href="/voeding/adminpanel.jsp">Admin paneel</a></li>
		      </ul>
		      <ul class="side-nav" id="mobile-demo">
				<li><a href="/voeding/profiel.jsp">Profiel</a></li>
		        <li><a href="/voeding/producten.jsp">Producten</a></li>
		        <li><a href="/voeding/adminpanel.jsp">Admin paneel</a></li>
      		</ul>
		    </div>
		    </div>
  		</nav>


<script>
 $(document).ready(function(){
   // Activate the side menu 
   $(".button-collapse").sideNav();
  });
</script>

  	
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	</head>
	<body>
	<jsp:include page="header.jsp"/>
	<p></p>
	<div class="container">
		<br>
		<div class="row">
			<form class="col s12" action="/LoginServlet.do" method="post">
			<div class="row">
			<br>
				<div class="input-field col s6">
				<label class="active" for="gebruikersnaam"><h5>Gebruikersnaam</h5></label>
				<input type="text" name="gebruikersnaam" value="${cookie.gebruikersnaam.value}" class="validate"/><br/>
				</div>
			</div>
			<div class="row">
			<br>
				<div class="input-field col s6">
				<label class="active"for="wachtwoord"><h5>Wachtwoord</h5></label>
				<input type="password" name="wachtwoord" class="validate"/><br/>
				</div>
			</div>
			<div class="row">
				<button class="btn-large" type="submit" name="action">Inloggen</button>
			</div>		
			</form>
		</div>
		
		<div id="messagebox">
		<%
			Object msgs = request.getAttribute("msgs");
			if (msgs != null) {
			out.println(msgs);
			}
		%>
		</div>
	
	</div>
		
	</body>
	<jsp:include page="footer.jsp"/>
</html>
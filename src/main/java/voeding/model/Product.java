package voeding.model;

public class Product {
	
	private int productId;
	private String naam;
	private String status;
	private String categorie;
	private String omschrijving;
	
	// Product constructor
	public Product(int productId, String naam, String status, String categorie, String omschrijving){
		this.productId = productId;
		this.naam = naam;
		this.status = status;
		this.categorie = categorie;
		this.omschrijving = omschrijving;
	}
	
	// alle getters en setters van Product
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getNaam() {
		return naam;
	}
	public void setNaam(String naam) {
		this.naam = naam;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getOmschrijving() {
		return omschrijving;
	}
	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}
	
	

}

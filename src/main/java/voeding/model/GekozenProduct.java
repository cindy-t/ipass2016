package voeding.model;

public class GekozenProduct {
	
	private int id;
	private Gebruiker gebruiker;
	private Product product;
	
	// GekozenProduct constructor
	public GekozenProduct(int id, Gebruiker gebruiker, Product product){
		this.id = id;
		this.gebruiker = gebruiker;
		this.product = product;
	}
	
	// alle getters en setters van GekozenProduct
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Gebruiker getGebruiker() {
		return gebruiker;
	}

	public void setGebruiker(Gebruiker gebruiker) {
		this.gebruiker = gebruiker;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	

	

}

package voeding.model;

import persistence.GebruikerService;
import persistence.GekozenProductService;
import persistence.ProductService;

public class VoedingServiceProvider {
	public VoedingServiceProvider() {
		// TODO Auto-generated constructor stub
	}
	
	private static GebruikerService voedingService = new GebruikerService();
	private static ProductService productService = new ProductService();
	private static GekozenProductService gekozenProductService = new GekozenProductService();
	
	// verkrijg een gebruikersservice als die nog niet bestaat
	public static GebruikerService getGebruikerService(){
		if (voedingService == null)
			voedingService = new GebruikerService();
		
		return voedingService;
	}
	
	// verkrijg een gekozenproductservice als die nog niet bestaat
	public static GekozenProductService getGekozenProductService(){
		if (gekozenProductService == null)
			gekozenProductService = new GekozenProductService();
		
		return gekozenProductService;
	}
	
	// verkrijg een productservice als die nog niet bestaat
	public static ProductService getProductService(){
		if (productService == null)
			productService = new ProductService();
		
		return productService;
	}
	


}

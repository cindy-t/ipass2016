package voeding.model;

public class Gebruiker {
	
	private int gebruikerId;
	private String gebruikersnaam;
	private String wachtwoord;
	private String voornaam;
	private String achternaam;
	private int leeftijd;
	
	// Gebruiker constructor 
	public Gebruiker(int gebruikerId, String gebruikersnaam, String wachtwoord, String voornaam, String achternaam, int leeftijd){
		this.gebruikerId = gebruikerId;
		this.setGebruikersnaam(gebruikersnaam);
		this.setWachtwoord(wachtwoord);
		this.setVoornaam(voornaam);
		this.setAchternaam(achternaam);
		this.setLeeftijd(leeftijd);
	}
	
	// alle getters en setters van Gebruiker
	public int getGebruikerId(){
		return gebruikerId;
	}
	
	public void setGebruikerId(int gebruikerId){
		this.gebruikerId = gebruikerId;
	}

	public String getGebruikersnaam() {
		return gebruikersnaam;
	}

	public void setGebruikersnaam(String gebruikersnaam) {
		this.gebruikersnaam = gebruikersnaam;
	}

	public boolean checkWachtwoord(String pass) {
		boolean check = false;
		
		if (this.wachtwoord.equals(pass))
			check = true;
		
		return check;
	}

	public void setWachtwoord(String wachtwoord) {
		this.wachtwoord = wachtwoord;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public int getLeeftijd() {
		return leeftijd;
	}

	public void setLeeftijd(int leeftijd) {
		this.leeftijd = leeftijd;
	}
	
	
	@Override
	public boolean equals(Object obj){
		boolean result = false;
		if(obj instanceof Gebruiker){
			Gebruiker andereGebruiker = (Gebruiker) obj;
			if(this.gebruikerId == andereGebruiker.gebruikerId
					&& this.gebruikersnaam.equals(andereGebruiker.gebruikersnaam)
					&& this.wachtwoord.equals(andereGebruiker.wachtwoord)
					&& this.voornaam.equals(andereGebruiker.voornaam)
					&& this.achternaam.equals(andereGebruiker.achternaam)
					&& this.leeftijd == andereGebruiker.leeftijd
					){
				result = true;
			}
		}
		return result;
	}
	
	

}

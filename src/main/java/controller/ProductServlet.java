package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import persistence.ProductService;
import voeding.model.Gebruiker;
import voeding.model.GekozenProduct;
import voeding.model.Product;
import voeding.model.VoedingServiceProvider;

/**
 * Servlet implementation class ProductServlet
 */
@WebServlet("/ProductServlet")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ProductService service = VoedingServiceProvider.getProductService();

		
		Product p = (Product) VoedingServiceProvider.getProductService().getAllProducten();
		req.setAttribute("product", p);
		RequestDispatcher rd = req.getRequestDispatcher("product.jsp");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		RequestDispatcher rd = null;
		
		// als de parameter toevoegen niet null is
		// verkrijg dan de sessie van de ingelogde gebruiker
		if (req.getParameter("toevoegen") != null) {
			Integer toevoegen = Integer.parseInt(req.getParameter("toevoegen"));
			Gebruiker g = (Gebruiker)session.getAttribute("loggedUser");
			
			// verkrijgt het gekozenproduct id
			GekozenProduct gp = VoedingServiceProvider.getGekozenProductService().getById(toevoegen);
			// slaat het gekozenproduct op 
			boolean succes = VoedingServiceProvider.getGekozenProductService().Create(toevoegen, g);
			
			req.setAttribute("succes", succes);
			
			rd = req.getRequestDispatcher("/voeding/producten.jsp");
			rd.forward(req, resp);	
			
		}

		
	}

}

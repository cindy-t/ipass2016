package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import voeding.model.Gebruiker;
import voeding.model.VoedingServiceProvider;

/**
 * Servlet implementation class LoginServlet
 */
@SuppressWarnings("serial")
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		String gebruikersnaam = req.getParameter("gebruikersnaam");
		String wachtwoord = req.getParameter("wachtwoord");
		HttpSession session = req.getSession();
		
		// controleert of de gebruikersnaam en het wachtwoord niet null zijn
		if(gebruikersnaam != null && wachtwoord != null){
			// controleert de gebruikersnaam en het wachtwoord overeen komen
			Gebruiker g = VoedingServiceProvider.getGebruikerService().checkGebruikerWachtwoord(gebruikersnaam, wachtwoord);
			
			// controleert of de gebruiker niet null is
			// en stuur de gebruiker door naar de profiel pagina
			if (g != null) {
			session.setAttribute("loggedUser", g);
			rd = req.getRequestDispatcher("/voeding/profiel.jsp");
			}	else {
				req.setAttribute("msgs", "gebruikersnaam of wachtwoord incorrect");
				rd = req.getRequestDispatcher("/index.jsp");
			}
		}
		// bij foute gebruikersinvoer
		else{
			req.setAttribute("msgs", "gebruikersnaam of wachtwoord incorrect");
			rd = req.getRequestDispatcher("/index.jsp");
		}
		rd.forward(req, resp);
	
	}

}

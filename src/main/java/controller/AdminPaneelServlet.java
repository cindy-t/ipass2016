package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import voeding.model.GekozenProduct;
import voeding.model.Product;
import voeding.model.VoedingServiceProvider;

/**
 * Servlet implementation class AdminPaneelServlet
 */
@WebServlet("/AdminPaneelServlet")
public class AdminPaneelServlet extends HttpServlet {

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminPaneelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("static-access")
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		RequestDispatcher rd = null;

		// als de parameter van opslaan niet null is
		// set dan alle attributen van product
		if (req.getParameter("opslaan") != null) {
		Integer bewerken = Integer.parseInt(req.getParameter("opslaan"));
		Product p = VoedingServiceProvider.getProductService().getProductById(bewerken);
		
		p.setNaam(req.getParameter("naam"));
		p.setCategorie(req.getParameter("categorie"));
		p.setStatus(req.getParameter("status"));
		p.setOmschrijving(req.getParameter("omschrijving"));
		
		boolean succes = VoedingServiceProvider.getProductService().Edit(p);
		
		req.setAttribute("succes", succes);
		
		rd = req.getRequestDispatcher("/voeding/adminpanel.jsp");
		rd.forward(req, resp);
		}
	}

}

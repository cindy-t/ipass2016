package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import voeding.model.Gebruiker;
import voeding.model.GekozenProduct;
import voeding.model.VoedingServiceProvider;

/**
 * Servlet implementation class ProfielServlet
 */
@WebServlet("/ProfielServlet")
public class ProfielServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfielServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		List<GekozenProduct> p = VoedingServiceProvider.getGekozenProductService().getAll();
		session.setAttribute("gekozenProducten", p);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		RequestDispatcher rd = null;
		
		// als de parameter toDelete niet null is
		// verkrijg dan de sessie van de ingelogde gebruiker
		if (req.getParameter("toDelete") != null) {
		Integer delete = Integer.parseInt(req.getParameter("toDelete"));
		Gebruiker g = (Gebruiker)session.getAttribute("loggedUser");
		
		// verkrijg het product id
		GekozenProduct p = VoedingServiceProvider.getGekozenProductService().getById(delete);
		// verwijderd het product uit de lijst
		VoedingServiceProvider.getGekozenProductService().Delete(p);
			
		resp.sendRedirect("/voeding/profiel.jsp");
		}
		
	}

}

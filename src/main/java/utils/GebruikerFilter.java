package utils;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class GebruikerFilter
 */
@WebFilter("/GebruikerFilter")
public class GebruikerFilter implements Filter {

    /**
     * Default constructor. 
     */
    public GebruikerFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest r1 = (HttpServletRequest)request;
		HttpServletResponse r2 = (HttpServletResponse)response;
		
		// verkijg een sessie en als het attribuut null is stuur de gebruiker terug naar de loginpagina
		if(r1.getSession().getAttribute("loggedUser") == null){
			r1.getRequestDispatcher("/index.jsp").forward(r1, r2);
		}
		else{
			// pass the request along the filter chain
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}

package persistence;

import java.util.ArrayList;
import java.util.List;

import voeding.model.*;



public class GekozenProductService {
	private List<GekozenProduct> alleGekozenProducten;
	private GekozenProductDAO  gekozenProductDao;
	
	// maak een nieuwe gekozenproductDAO aan en zoek alle gekozenproducten
	public GekozenProductService() {
		this.gekozenProductDao = new GekozenProductDAO();
		this.alleGekozenProducten = this.gekozenProductDao.findAll();
	}
	
	// zoek een gekozenproduct op id
	public GekozenProduct getById(int id) {
		GekozenProduct gp = null;
		
		for (GekozenProduct item: this.alleGekozenProducten) {
			if (item.getId() == id)
				gp = item;
		}
		return gp;
	}
	
	// verkrijg alle gekozenproducten 
	public List<GekozenProduct> getAll() {
		return this.alleGekozenProducten;
	}
	
	// verkrijg een gebruiker uit de gekozenproducten lijst
	public List<GekozenProduct> GetByGebruiker(Gebruiker g){
		List<GekozenProduct> result = new ArrayList<GekozenProduct>();
		
		for (GekozenProduct item: this.alleGekozenProducten) {
			if (item.getGebruiker().equals(g)) {
				result.add(item);
			}
		}
		
		return result;
	}
	
	// verwijderd het gekozenproduct uit de lijst
	public boolean Delete(GekozenProduct gp) {
		boolean succes = false;
				if (this.gekozenProductDao.deleteGekozenProduct(gp)){
					this.alleGekozenProducten.remove(gp);
					succes = true;
				}
		return succes;
	}
	
	// voegt een product id en gebruiker id toe aan de gekozenproducten lijst
	public boolean Create(int product_id, Gebruiker g){
		boolean succes = false;
		
		Product p = VoedingServiceProvider.getProductService().getProductById(product_id);
		
		if (p != null) {
			
			int id = 0;
			GekozenProduct gp = new GekozenProduct(id, g, p);
			boolean exists = false;
			for (GekozenProduct item : this.alleGekozenProducten) {
				if (id < item.getId())
					id = item.getId();
				if (item.getGebruiker().equals(gp.getGebruiker()) && item.getProduct().equals(gp.getProduct())) {
					exists = true;
				}
			}
			if (exists == false) {
				gp.setId(id + 1);
			if(this.gekozenProductDao.createGekozenProduct(gp)) {
				this.alleGekozenProducten.add(gp);
				succes = true;
			}
			}
		}
		return succes;
	}

}

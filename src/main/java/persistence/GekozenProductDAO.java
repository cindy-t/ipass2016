package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import voeding.model.Gebruiker;
import voeding.model.GekozenProduct;
import voeding.model.Product;
import voeding.model.VoedingServiceProvider;



public class GekozenProductDAO extends BaseDAO{
	
	// doorloop alle rijen van de gekozenproducten tabel
	// voegt daarna het nieuwe gekozenproduct toe aan de lijst
	private List<GekozenProduct> selectGekozenProducten(String query){
		List<GekozenProduct> results = new ArrayList<GekozenProduct>();
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while (dbResultSet.next()) {
				int gekozen_product_id = dbResultSet.getInt("id_gekozen_product");
				
				Gebruiker gebruiker = VoedingServiceProvider.getGebruikerService().getGebruikerById(dbResultSet.getInt("gebruiker_id"));
				Product product = VoedingServiceProvider.getProductService().getProductById(dbResultSet.getInt("product_id"));
				
				GekozenProduct newGekozenProduct = new GekozenProduct(gekozen_product_id, gebruiker, product);
				
				results.add(newGekozenProduct);
			}
		} catch(SQLException sqle){
			sqle.printStackTrace();
		}
		
		return results;
	}
	
	// zoek alle gekozenproducten uit de database
	public List<GekozenProduct> findAll(){
		return selectGekozenProducten("SELECT * FROM gekozen_product");
	}
	
	// zoek een gekozenproduct op gekozenproduct id in de database
	public GekozenProduct findById(int gekozenProductId) {
		return selectGekozenProducten("SELECT * FROM gekozen_product WHERE id_gekozen_product = " + gekozenProductId).get(0);
	}
	
	// verwijderd het gekozenproduct uit de database
	public boolean deleteGekozenProduct(GekozenProduct gp){
		boolean result = false;
	
			String query = "DELETE FROM gekozen_product WHERE product_id = " 
					+ gp.getProduct().getProductId()
					+ " AND gebruiker_id=" 
					+ gp.getGebruiker().getGebruikerId()
					+ " AND id_gekozen_product = " 
					+ gp.getId();
			
			try (Connection con = super.getConnection()) {
				Statement stmt = con.createStatement();
				
				if (stmt.executeUpdate(query) == 1){
					
					System.out.println("Product id: " + gp.getProduct().getProductId());
					System.out.println("Gebruiker id: " + gp.getGebruiker().getGebruikerId());
					System.out.println(" verwijderd");
					
					
				}
				result = true;
			} catch(SQLException sqle){
				sqle.printStackTrace();
			}
		return result;
	}
	
	// voegt een gekozenproduct toe in de database
	public boolean createGekozenProduct(GekozenProduct gp){
		boolean result = false;
		String query = "INSERT INTO gekozen_product values("
						+ gp.getId()
						+ ","
						+ gp.getGebruiker().getGebruikerId()
						+ ","
						+ gp.getProduct().getProductId()
						+ ")";
		System.out.println(query);
		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			
			if(stmt.executeUpdate(query) == 1){
				System.out.println("Toegevoegd: GproductId: " + gp.getId());
				System.out.print(" productId: " + gp.getProduct().getProductId());
				System.out.print("gebruikerId: " + gp.getGebruiker().getGebruikerId());
				result = true;
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return result;
	}
	

	

}

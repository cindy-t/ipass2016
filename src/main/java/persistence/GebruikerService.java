package persistence;

import java.util.List;

import voeding.model.Gebruiker;

public class GebruikerService {

	private GebruikerDAO gebruikerDAO;
	private List<Gebruiker> alleGebruikers;
	
	// maak een nieuwe gebruikerDAO aan en verkrijg alle gebruikers
	public GebruikerService() {
		this.gebruikerDAO = new GebruikerDAO();
		this.alleGebruikers = this.gebruikerDAO.getAll();
	}
	
	// verkrijg alle gebruikers
	public List<Gebruiker> getAll() {
		return this.alleGebruikers;
	}
	
	// controleert de gebruikersnaam en het wachtwoord van een gebruiker
	public Gebruiker checkGebruikerWachtwoord(String gebruikersnaam, String wachtwoord) {
		Gebruiker g = null;
		for (Gebruiker item: this.alleGebruikers) {
			if (item.checkWachtwoord(wachtwoord)) {
				g = item;
			}
		}
		return g;
	}
	
	// verkrijg een gebruiker bij gebruikers id
	public Gebruiker getGebruikerById(int id) {
		Gebruiker g = null;
		for (Gebruiker item: this.alleGebruikers) {
			if (item.getGebruikerId() == id) {
				g = item;
			}
		}
		return g;
	}
	
}

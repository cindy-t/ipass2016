package persistence;

import java.sql.Connection;
import javax.naming.InitialContext;
import javax.sql.DataSource;



public class BaseDAO {
	
	// verkrijg een connectie met de database
	protected final Connection getConnection(){
		Connection result = null;
		
		try{
			InitialContext ic = new InitialContext();
			DataSource ds = (DataSource) ic.lookup("java:comp/env/jdbc/MySQLDS");
			result = ds.getConnection();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		// return het resultaat van de connectie
		return result;
	}

}

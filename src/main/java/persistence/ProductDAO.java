package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import voeding.model.Product;

public class ProductDAO extends BaseDAO{
	
	// doorloop alle rijen van de producten tabel
	// voegt daarna het nieuwe product toe aan de lijst
	public List<Product> getProducten() {
		List<Product> result = new ArrayList<>();
		
		String query = "SELECT * FROM producten";
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while (dbResultSet.next()) {
				int product_id = dbResultSet.getInt("product_id");
				String naam = dbResultSet.getString("naam");
				String status = dbResultSet.getString("product_status");
				String categorie = dbResultSet.getString("categorie");
				String omschrijving = dbResultSet.getString("omschrijving");
				
				
				Product newProduct = new Product(product_id, naam, status, categorie, omschrijving);
				
				result.add(newProduct);
			}
		} catch(SQLException sqle){
			System.out.println(sqle);
		}
		
		return result;
	}
	
	// verkrijg het product id uit de lijst waar het gekozenproduct id en gebruiker id gelijk is aan het product id
	public List<Product> getProducten(int id) {
		List<Product> result = new ArrayList<>();
		
		String query = "SELECT * FROM gekozen_product as g join producten as p on (g.product_id = p.product_id) WHERE g.gebruiker_id = " + id;
		
		System.out.println(query);
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while (dbResultSet.next()) {
				int product_id = dbResultSet.getInt("id_gekozen_product");
				String naam = dbResultSet.getString("naam");
				String status = dbResultSet.getString("product_status");
				String categorie = dbResultSet.getString("categorie");
				String omschrijving = dbResultSet.getString("omschrijving");
				
				
				Product newProduct = new Product(product_id, naam, status, categorie, omschrijving);
				
				result.add(newProduct);
			}
		} catch(SQLException sqle){
			System.out.println(sqle);
		}
		
		return result;
	}
	
	// doorloop alle rijen van de producten tabel
	// voegt daarna het nieuwe product toe aan de lijst
	private List<Product> selectProducten(String query){
		List<Product> results = new ArrayList<Product>();
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while (dbResultSet.next()) {
				int product_id = dbResultSet.getInt("product_id");
				String naam = dbResultSet.getString("naam");
				String status = dbResultSet.getString("product_status");
				String categorie = dbResultSet.getString("categorie");
				String omschrijving = dbResultSet.getString("omschrijving");
				
				
				Product newProduct = new Product(product_id, naam, status, categorie, omschrijving);
				
				results.add(newProduct);
			}
		} catch(SQLException sqle){
			sqle.printStackTrace();
		}
		
		return results;
	}
	
	// zoek het product op product id in de database
	public Product findById(int product_id) {
		return selectProducten("SELECT * FROM producten WHERE product_id = " + product_id).get(0);
	}
	
	// zoek alle producten in de database
	public List<Product> findAll(){
		return selectProducten("SELECT * FROM producten");
	}
	
	// wijzigt een product in de database
	public boolean editProduct(Product p){
		boolean result = false;
		String query = "UPDATE producten SET" 
						+ " naam = '"
						+ p.getNaam()
						+ "', product_status = '"
						+ p.getStatus()
						+ "', categorie = '"
						+ p.getCategorie()
						+ "', omschrijving = '"
						+ p.getOmschrijving()
						+ "' WHERE product_id = " + p.getProductId();
		System.out.println(query);
		try (Connection con = super.getConnection()) {
			Statement stmt = con.createStatement();
			
			if(stmt.executeUpdate(query) == 1){
				result = true;
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return result;
	}

}

package persistence;

import java.util.List;
import voeding.model.Product;

public class ProductService {
	
	private List<Product> alleProducten;
	private ProductDAO productDAO;
	
	// maak een nieuwe productDAO aan en zoek alle producten
	public ProductService() {
		this.productDAO = new ProductDAO();
		this.alleProducten = this.productDAO.findAll();
	}
	
	// verkrijg alle producten
	public List<Product> getAllProducten(){
		return this.alleProducten;
	}
	
	// verkrijg het product op product id uit de lijst
	public Product getProductById(int id) {
		Product p = null;
		for (Product item: this.alleProducten) {
			if (item.getProductId() == id) {
				p = item;
			}
		}
		return p;
	}
	
	// wijzigt het product uit de lijst
	public boolean Edit(Product p){
		boolean succes = false;
		
		for (Product item : this.alleProducten) {
			if (item.getProductId() == p.getProductId()) {
				if (this.productDAO.editProduct(p)) {
					this.alleProducten.remove(item);
					this.alleProducten.add(p);
					succes = true;
				}
			}
		}
		
		return succes;
	}
		
}

package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import voeding.model.Gebruiker;

public class GebruikerDAO extends BaseDAO{
	
	// doorloop alle rijen van de gebruiker tabel
	// voegt daarna de nieuwe gebruiker toe aan de lijst
	private List<Gebruiker> selectGebruikers(String query){
		List<Gebruiker> results = new ArrayList<Gebruiker>();
		
		try(Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			
			while (dbResultSet.next()) {
				int gebruiker_id = dbResultSet.getInt("gebruiker_id");
				String gebruikersnaam = dbResultSet.getString("gebruikersnaam");
				String wachtwoord = dbResultSet.getString("wachtwoord");
				String voornaam = dbResultSet.getString("voornaam");
				String achternaam = dbResultSet.getString("achternaam");
				int leeftijd = dbResultSet.getInt("leeftijd");
				
				
				Gebruiker newGebruiker = new Gebruiker(gebruiker_id,gebruikersnaam,wachtwoord,voornaam,achternaam,leeftijd);
				
				results.add(newGebruiker);
			}
		} catch(SQLException sqle){
			sqle.printStackTrace();
		}
		
		return results;
	}
	
	// verkrijg alle gebruikers in de database
	public List<Gebruiker> getAll() {
		return selectGebruikers("SELECT * FROM gebruiker");
	}
	
	// zoek een gebruiker op gebruikers id in de database
	public Gebruiker findById(int gebruiker_id) {
		return selectGebruikers("SELECT * FROM gebruiker WHERE gebruiker_id = " + gebruiker_id).get(0);
	}
	
}

package voedingsapp.services;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import voeding.model.Gebruiker;
import voeding.model.VoedingServiceProvider;

@Path("/gebruikers")
public class GebruikerResources {
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public String getGebruiker(@PathParam("id") int id){

		Gebruiker gebruiker = VoedingServiceProvider.getGebruikerService().getGebruikerById(id);
		JsonObjectBuilder job = Json.createObjectBuilder();
		
		job.add("gebruiker_id", gebruiker.getGebruikerId());
		job.add("voornaam", gebruiker.getVoornaam());
		return job.build().toString();
	}
	

}
